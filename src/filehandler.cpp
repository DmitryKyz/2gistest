#include "filehandler.h"
#include <fstream>
#include <unordered_set>

#include <iostream>

FileHandler::FileHandler(const std::string &filePath): filePath(filePath)
{
}

/*
 * Возвращает 32-битную чексумму файла
 */
uint32_t FileHandler::checksum()
{
    auto stream = createIFStream();
    uint32_t cSum = 0;

    while (stream)
    {
        uint32_t n = 0;
        stream.read(reinterpret_cast<char*>(&n), sizeof n);
        cSum += n;
    }

    return cSum;
}

/*
 * Возвращает количество слов word, встречающихся в файле
 *
 * Параметры:
 * - word - слово, количество которых требуется посчитать
 */
int FileHandler::countWords(const std::string &word)
{
    if (word.empty())
        return 0;

    auto stream = createIFStream();

    int count = 0;
    while (stream)
    {

        std::string w;
        stream >> w;
        if (word == trimmedWord(w))
            count++;
    }

    return count;
}

/*
 * Возвращает поток чтения из файла
 * может вызывать исключение std::runtime_error
 */
std::ifstream FileHandler::createIFStream()
{
    std::ifstream stream(filePath);
    if (!stream)
        throw std::runtime_error("Could not open file!");
    return stream;
}

/*
 * Возвращает обрезанное слева и аправа от знаков препинания слово
 * слово обрезается от {',', '.', '!', '?', '\'', '\"', ':', ';'} знаков.
 *
 * Параметры:
 * - word - слово, которое требуется обрезать
 */
std::string FileHandler::trimmedWord(const std::string &word)
{
    std::unordered_set<char> st{',', '.', '!', '?', '\'', '\"', ':', ';'};

    auto left = word.begin();
    while (left != word.end() && st.find(*left) != st.end())
        left++;

    auto right = word.end();
    if (left != word.end())
    {
        while (left != right-1 && st.find(*(right-1)) != st.end())
            right--;
    }

    return {left, right};
}
