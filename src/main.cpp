#include <iostream>
#include <iomanip>
#include <cstring>

#include "filehandler.h"
#include "parameterparser.h"

/*
 * Выполнил: Кузнецов Д.Ю.
 * E-mail: seidenpochta@gmail.com
 */

void printHelp();

int main(int argc, char *argv[])
{
    try {
        ParameterParser parcer;
        auto params = parcer.parse(argc, argv);

        switch (parcer.command()) {
        case ParserCommand::Help:
            printHelp();
            break;
        case ParserCommand::Checksum:
        {
            auto chSum = FileHandler(params["file"]).checksum();
            std::cout << "Checksum is: " << chSum << std::endl;
        }
            break;
        case ParserCommand::Words:
        {
            auto count = FileHandler(params["file"]).countWords(params["value"]);
            std::cout << "Word count is: " << count << std::endl;
        }
            break;
        default:
            throw std::runtime_error("Unknow command!");
            break;
        }
    }
    catch (std::runtime_error &err) {
        std::cout << err.what() << "\n";
        return 1;
    }
    catch (...) {
        std::cout << "An unknown error occurred!\n";
        return 1;
    }

    return 0;
}

/*
 * Печатает справку по программе
 */
void printHelp()
{
    using namespace std;
    constexpr int w = 20;

    cout << "The program works in three modes:\n"

         << "Mode 1 - counts the number of words in the file\n"
         << "Using:\n"
         << "  test -f [file_path] -m words -v [word]\n\n"

         << "Mode 2 - counts the 32-bit checksum of the file\n"
         << "Using:\n"
         << "  test -f [file_path] -m checksum\n\n"

         << "Mode 3 - print the program help\n"
         << "Using:\n"
         << "  test -h\n\n"

         << "Parameters:\n"
         << setw(w) << left << "-m [words/checksum]" << "command parameter\n"
         << setw(w) << left << "-f [file_path]"      << "path to file\n"
         << setw(w) << left << "-v [word]"           << "the word to be counted in the file\n"
         << setw(w) << left << "-h"                  << "print help\n";
}
