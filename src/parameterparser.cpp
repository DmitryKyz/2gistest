#include "parameterparser.h"
#include <filesystem>
#include <cstring>

#include <iostream>

const std::string ParameterParser::PARAM_ERR_MSG = "Incorrect parameters passed!";

/*
 * Проводит разбор параметров.
 *
 * Параметры:
 * - argv - массив параметров
 * - argc - размер массива параметров
 *
 * Возвращает список параметров разбитых на ключ-значение.
 * Ключи:
 *  - command - комманда программе (help, checksum, words)
 *  - file - путь к файлу
 *  - value - значение для выполнения комманды
 */
std::map<std::string, std::string> ParameterParser::parse(int argc, char *argv[])
{
    std::map<std::string, std::string> parameters;
    cmd = ParserCommand::No;

    if (argc == 2)
    {
        if (strcmp(argv[1], "-h") == 0)
        {
            parameters["command"] = "help";
            cmd = ParserCommand::Help;
        }
        else
            throw std::runtime_error(PARAM_ERR_MSG);
    }
    else if (argc == 5)
    {
        checkPath(argv[1], argv[2]);
        parameters["file"] = argv[2];

        if (strcmp(argv[3], "-m") != 0 || strcmp(argv[4], "checksum") != 0)
            throw std::runtime_error(PARAM_ERR_MSG);

        parameters["command"] = "checksum";
        cmd = ParserCommand::Checksum;
    }
    else if (argc == 7)
    {
        checkPath(argv[1], argv[2]);
        parameters["file"] = argv[2];

        if (strcmp(argv[3], "-m") != 0 || strcmp(argv[4], "words") != 0 ||
                 strcmp(argv[5], "-v"))
            throw std::runtime_error(PARAM_ERR_MSG);

        parameters["command"] = "words";
        parameters["value"] = argv[6];
        cmd = ParserCommand::Words;
    }
    else
        throw std::runtime_error(PARAM_ERR_MSG);

    return parameters;
}

/*
 * Проверяет переданный ключ и строку пути.
 * Проверяет файл на существование, и имеет ли он поддерживаемый формат
 * В случае провала проверки вызывает исключение std::runtime_error
 *
 * Параметры:
 * - key - ключ, указывающий на передачу пути файла
 * - pathStr - строка пути к файлу
 */
void ParameterParser::checkPath(char *key, char *pathStr)
{
    namespace fs = std::filesystem;

    if (strcmp(key, "-f") != 0)
        throw std::runtime_error(PARAM_ERR_MSG);

    fs::path dir(pathStr);

    if (!exists(dir))
        throw std::runtime_error("File not exists!");

    if (dir.extension() != ".tst")
        throw std::runtime_error("Unsupported file format transferred!");
}

/*
 * Возвращает переданную в параметрах комманду
 * Если разбор параметров еще не происходил, возвращает ParserCommand::No
 */
ParserCommand ParameterParser::command()
{
    return cmd;
}
