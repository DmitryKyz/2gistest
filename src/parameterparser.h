#ifndef PARAMETERPARSER_H
#define PARAMETERPARSER_H

#include <map>
#include <string>

enum class ParserCommand {Help, Checksum, Words, No};

/*
 * Класс ParameterParser
 * Предназначен для разбора массива переданных в программу параметров
 */
class ParameterParser
{
public:
    static const std::string PARAM_ERR_MSG;

    std::map<std::string, std::string> parse(int argc, char *argv[]);
    ParserCommand command();

private:
    ParserCommand cmd = ParserCommand::No;

    void checkPath(char *key, char *pathStr);
};

#endif // PARAMETERPARSER_H
