#ifndef FILEHANDLER_H
#define FILEHANDLER_H

#include <string>

/*
 * Класс FileHandler
 * Предназначен для подсчета количества слов и
 * чексуммы и переданного файла
 */
class FileHandler
{
public:
    FileHandler(const std::string &filePath);
    uint32_t checksum();
    int countWords(const std::string &word);

private:
    std::ifstream createIFStream();
    std::string trimmedWord(const std::string &word);
    std::string filePath;
};

#endif // FILEHANDLER_H
