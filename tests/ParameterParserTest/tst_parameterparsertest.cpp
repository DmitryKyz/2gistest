#include <QtTest>
#include <cstring>
#include <memory>
#include <functional>
#include <filesystem>

#include <iostream>

#include "../../src/parameterparser.h"

using deleter = std::function<void(char*[])>;
namespace fs = std::filesystem;

class ParameterParserTest : public QObject
{
    Q_OBJECT

public:
    ParameterParserTest();
    ~ParameterParserTest();

private slots:
    void test_parceMode1();
    void test_parceMode2();
    void test_parceMode3();
    void test_uncurrentParameters();
    void test_uncurrentFormatPath();
    void test_notExistsPath();

private:
    ParameterParser parser;
    std::string path;

    std::unique_ptr<char*[], deleter> getParams(std::vector<std::string> &vct);
};

ParameterParserTest::ParameterParserTest()
{
    fs::path p = "..";
    p = p / "files" / "words.tst";
    path = p;
}

ParameterParserTest::~ParameterParserTest()
{

}

void ParameterParserTest::test_parceMode1()
{
    std::vector<std::string> vct{"test_prog", "-f", path, "-m", "checksum"};
    auto params = parser.parse(vct.size(), getParams(vct).get());

    QCOMPARE(parser.command(), ParserCommand::Checksum);
    QCOMPARE(params["command"], std::string("checksum"));
    QCOMPARE(params["file"], path);
}

void ParameterParserTest::test_parceMode2()
{
    std::string value("word");
    std::vector<std::string> vct{"test_prog", "-f", path, "-m", "words", "-v", value};
    auto params = parser.parse(vct.size(), getParams(vct).get());

    QCOMPARE(parser.command(), ParserCommand::Words);
    QCOMPARE(params["command"], std::string("words"));
    QCOMPARE(params["file"], path);
    QCOMPARE(params["value"], std::string(value));
}

void ParameterParserTest::test_parceMode3()
{
    std::vector<std::string> vct{"test_prog", "-h"};
    auto params = parser.parse(vct.size(), getParams(vct).get());

    QCOMPARE(parser.command(), ParserCommand::Help);
    QCOMPARE(params["command"], std::string("help"));
}

void ParameterParserTest::test_uncurrentParameters()
{
    std::vector<std::string> vct{"test_prog"};
    QVERIFY_EXCEPTION_THROWN(parser.parse(vct.size(), getParams(vct).get()), std::runtime_error);

    vct = {"test_prog", "-w"};
    QVERIFY_EXCEPTION_THROWN(parser.parse(vct.size(), getParams(vct).get()), std::runtime_error);

    vct = {"test_prog", "-f", path};
    QVERIFY_EXCEPTION_THROWN(parser.parse(vct.size(), getParams(vct).get()), std::runtime_error);

    vct = {"test_prog", "-f", path, "-m"};
    QVERIFY_EXCEPTION_THROWN(parser.parse(vct.size(), getParams(vct).get()), std::runtime_error);

    vct = {"test_prog", "-f", path, "-m", "words"};
    QVERIFY_EXCEPTION_THROWN(parser.parse(vct.size(), getParams(vct).get()), std::runtime_error);

    vct = {"test_prog", "-f", path, "-m", "checksum", "v"};
    QVERIFY_EXCEPTION_THROWN(parser.parse(vct.size(), getParams(vct).get()), std::runtime_error);

    vct = {"test_prog", "-m", "checksum", "-f", path};
    QVERIFY_EXCEPTION_THROWN(parser.parse(vct.size(), getParams(vct).get()), std::runtime_error);
}

void ParameterParserTest::test_uncurrentFormatPath()
{
    fs::path uncurrentFormat = "..";
    uncurrentFormat = uncurrentFormat / "files" / "notsupport.doc";

    std::vector<std::string> vct{"test_prog", "-f", uncurrentFormat, "-m", "checksum"};
    QVERIFY_EXCEPTION_THROWN(parser.parse(vct.size(), getParams(vct).get()), std::runtime_error);

    vct = {"test_prog", "-f", uncurrentFormat, "-m", "words", "-v", "word"};
    QVERIFY_EXCEPTION_THROWN(parser.parse(vct.size(), getParams(vct).get()), std::runtime_error);
}

void ParameterParserTest::test_notExistsPath()
{
    std::string notExistsPath = "../qwerty.tst";

    std::vector<std::string> vct{"test_prog", "-f", notExistsPath, "-m", "checksum"};
    QVERIFY_EXCEPTION_THROWN(parser.parse(vct.size(), getParams(vct).get()), std::runtime_error);

    vct = {"test_prog", "-f", notExistsPath, "-m", "words", "-v", "word"};
    QVERIFY_EXCEPTION_THROWN(parser.parse(vct.size(), getParams(vct).get()), std::runtime_error);
}

/*
 * создает массив c-type строк из вектора и возвращает unique_ptr на массив
 */
std::unique_ptr<char *[], deleter> ParameterParserTest::getParams(std::vector<std::string> &vct)
{
    deleter d = [s = vct.size()](char* argv[]){
        for (size_t i = 0; i < s; i++)
            delete[] argv[i];
        delete[] argv;
    };

    auto ptr = std::unique_ptr<char*[], deleter>(new char*[vct.size()], d);

    for (size_t i = 0; i < vct.size(); i++)
    {
        ptr.get()[i] = new char [vct[i].size()+1];
        strcpy(ptr.get()[i], vct[i].c_str());
    }

    return ptr;
}

QTEST_APPLESS_MAIN(ParameterParserTest)

#include "tst_parameterparsertest.moc"
