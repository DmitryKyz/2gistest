cmake_minimum_required(VERSION 3.8)

project(tests LANGUAGES CXX)

add_subdirectory("FileHandlerTest")
add_subdirectory("ParameterParserTest")

if (NOT CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_CURRENT_BINARY_DIR)
    FILE(COPY files DESTINATION "${CMAKE_BINARY_DIR}")
endif()
