#include <QtTest>
#include <string>
#include <fstream>
#include <filesystem>

#include "../../src/filehandler.h"

namespace fs = std::filesystem;

class FileHandlerTest : public QObject
{
    Q_OBJECT

public:
    FileHandlerTest();
    ~FileHandlerTest();
    uint32_t checksum(std::string &file);

private slots:
    void test_notExistsFile();
    void test_countWords();
    void test_checksum();

private:
    std::string emptyFile;
    std::string oneCharFile;
    std::string file;

};

FileHandlerTest::FileHandlerTest()
{
    fs::path p = "..";
    emptyFile = p / "files" / "empty.tst";
    oneCharFile = p / "files" / "one_char.tst";
    file = p / "files" / "words.tst";
}

FileHandlerTest::~FileHandlerTest()
{

}

void FileHandlerTest::test_notExistsFile()
{
    FileHandler fh {"12345.tst"};
    QVERIFY_EXCEPTION_THROWN(fh.checksum(), std::runtime_error);
    QVERIFY_EXCEPTION_THROWN(fh.countWords("word"), std::runtime_error);
    //fh.countWords("");
}

void FileHandlerTest::test_countWords()
{
    FileHandler fh{emptyFile};
    QCOMPARE(fh.countWords(""), 0);
    QCOMPARE(fh.countWords(" "), 0);
    QCOMPARE(fh.countWords("word"), 0);

    FileHandler fh1{file};
    QCOMPARE(fh1.countWords(""), 0);
    QCOMPARE(fh1.countWords(" "), 0);
    QCOMPARE(fh1.countWords("word"), 5);
    QCOMPARE(fh1.countWords("WORD"), 1);
}

void FileHandlerTest::test_checksum()
{
    FileHandler fh{emptyFile};
    QCOMPARE(fh.checksum(), checksum(emptyFile));

    FileHandler fh1{oneCharFile};
    QCOMPARE(fh1.checksum(), checksum(oneCharFile));

    FileHandler fh2{file};
    QCOMPARE(fh2.checksum(), checksum(file));
    QCOMPARE(fh2.checksum(), checksum(file));
}

uint32_t FileHandlerTest::checksum(std::string &file)
{
    /*
     * Возвращает 32-битную чексумму файла
     */

    std::ifstream stream(file);
    if (!stream)
        throw std::runtime_error("Could not open file!");

    uint32_t cSum = 0;

    while (stream)
    {
        uint32_t n = 0;
        stream.read(reinterpret_cast<char*>(&n), sizeof n);
        cSum += n;
    }

    return cSum;
}

QTEST_APPLESS_MAIN(FileHandlerTest)

#include "tst_filehandlertest.moc"
